<?php

//  blogRight! version 2.5 (2010-04-19)
//  Copyright (C) 2004, 2005, 2006, 2007, 2008, 2010 Wojciech Polak.
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the
//  Free Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program.  If not, see <http://www.gnu.org/licenses/>.

if (!ereg ("^apache2", @php_sapi_name ()))
  ob_start ('ob_gzhandler');

require 'config.php';

if (!isset ($CONF)) exit;

if (!isset ($CONF['path.header_file']))
  $CONF['path.header_file'] = 'inc/header.html';

if (!isset ($CONF['path.footer_file']))
  $CONF['path.footer_file'] = 'inc/footer.html';

if (!isset ($CONF['charsetEncoding']))
  $CONF['charsetEncoding'] = 'UTF-8';

/////////////////////////////

$start_time = microtime ();

if (isset ($_GET['q']))
{
  $q = $_GET['q'];
  $result = array ();
  ereg ('([0-9]{4})?[-/]?([0-1][0-9])?[-/]?([0-3][0-9])?', $q, $result);

  if (isset ($result[1]) && $result[1] != '')
  {
    $search_y = $result[1];

    if (isset ($result[2]) && $result[2] != '')
      $search_m = $result[2] <= 12 ? $result[2] > 0 ? $result[2] : 1 : 12;

    if (isset ($result[3]) && $result[3] != '')
      $search_d = $result[3] <= 31 ? $result[3] > 0 ? $result[3] : 1 : 31;
  }
  else
  {
    if (eregi ('^all$', $q))
      unset ($CONF['recent']);
  }
}

/////////////////////////////

if (isset ($CONF['path.data']) && $CONF['path.data'] != '')
{
  $cwd = $CONF['path.data'];
}
else
{
  $CONF['path.data'] = '';
  $cwd = getcwd ();
}

$feed_titles_idx = array ();
if (isset ($CONF['feed.titles.idx']))
  @include $CONF['path.data'].$CONF['feed.titles.idx'];

if (isset ($CONF['orderfile']) &&
    file_exists ($CONF['path.data'].$CONF['orderfile']))
{
  $lines = file ($CONF['path.data'].$CONF['orderfile']);
  foreach ($lines as $line)
    $searchdirs[] = chop ($line);
  $searchdirs = array_unique ($searchdirs);
}
else if (isset ($search_y))
{
  $searchdirs[] = $search_y;
}
else // scan the directory
{
  if (!($dp = opendir ($cwd)))
    die ("Can't open data directory.");
  $searchdirs = array ();
  while ($name = readdir ($dp))
  {
    if (is_numeric ($name))
      $searchdirs[] = $name;
  }
  closedir ($dp);
  sort ($searchdirs);

  if (isset ($CONF['sortorder']) &&
      $CONF['sortorder'] == 'desc')
    $searchdirs = array_reverse ($searchdirs);
}

/////////////////////////////

$include_files = array ();

foreach ($searchdirs as $dir)
{
  $path = $CONF['path.data'].$dir;
  $dirlist = array ();

  // ignore hidden files/dirs.
  if ($dir[0] != '.' && $dir[0] != '' && is_dir ($path))
  {
    if (file_exists ($path.'/'.$CONF['orderfile']))
    {
      $lines = file ($path.'/'.$CONF['orderfile']);
      foreach ($lines as $line)
	$dirlist[] = $path.'/'.chop ($line);
    }
    else // scan the directory
    {
      if (!($dp = opendir ($path)))
	die ("Can't open data subdirectory.");
      else
      {
	$i = 0;
	while ($name = readdir ($dp))
	{
	  if ($name[0] != '.' && $name != 'index.html')
	  {
	    $dirlist[filemtime ($path.'/'.$name) . $i] = $path.'/'.$name;
	    $i++;
	  }
	}
	closedir ($dp);

	if ($CONF['sortby'] == 'timestamp')
	  ksort ($dirlist);
	else // sort by name
	  sort ($dirlist);
      }
    }

    if ($CONF['sortorder'] == 'desc' && !isset ($search_y))
      $dirlist = array_reverse ($dirlist);

    $include_files = array_merge ($include_files, $dirlist);
  }
}

/////////////////////////////

$cur_y = date ('Y');
$cur_m = date ('m');

function cmonth ($var)
{
  global $cur_y, $cur_m;
  if (ereg ("$cur_y/$cur_m-.*\.html\$", $var))
    return true;
  else
    return false;
}

if (isset ($_GET['format'])) {
  if ($_GET['format'] == 'atom')
    $ATOM = true;
  else if ($_GET['format'] == 'rss')
    $RSS  = true;
}

if (isset ($ATOM) || isset ($RSS)) {
  $CONF['recent'] = $CONF['feed.recent'];
}

// show only N recent entries or current month
if (!isset ($search_y) && isset ($CONF['recent']))
{
  if (is_numeric ($CONF['recent']))
  {
    $include_files = array_slice ($include_files, 0, $CONF['recent']);
  }
  else if ($CONF['recent'] == 'month')
  {
    $month_files = array_filter ($include_files, 'cmonth');

    while (!count ($month_files))
    {
      $cur_m = sprintf ('%02d', $cur_m - 1);
      if ($cur_m == '00')
      {
	$cur_m = 12;
	$cur_y -= 1;
	if ($cur_y < 1983)
	  break; // fatal error; anti-loop protection
      }
      $month_files = array_filter ($include_files, 'cmonth');
    }

    $include_files = array ();
    foreach ($month_files as $val)
      $include_files[] = $val;
  }
}

/////////////////////////////

$year_array = array (); // Array of years for copyright purposes

if (isset ($RSS))
{
  $pubDate = gmdate ("D, d M Y H:i:s O", filemtime ($include_files[0]));
  $lastModified = gmdate ("D, d M Y H:i:s T", filemtime ($include_files[0]));
  $ETag = '"'.md5 ($lastModified).'"';

  $headers = getallheaders ();
  if (array_key_exists ('If-Modified-Since', $headers) &&
      array_key_exists ('If-None-Match', $headers))
  {
    if ($headers['If-Modified-Since'] == $lastModified &&
	$headers['If-None-Match'] == $ETag)
    {
      header ('HTTP/1.1 304 Not Modified');
      exit ();
    }
  }
  header ("Content-Type: text/xml; charset=".$CONF['charsetEncoding']);
  header ("Last-Modified: $lastModified");
  header ("ETag: $ETag");

  echo '<?xml version="1.0" encoding="'.$CONF['charsetEncoding'].'"?>'."\n";
  echo '<rss version="2.0">'."\n";
  echo "   <channel>\n";
  echo "     <title>".$CONF['feed.title']."</title>\n";
  echo "     <description>".$CONF['feed.description']."</description>\n";
  echo "     <link>".$CONF['url.site']."/</link>\n";
  echo "     <copyright>".$CONF['feed.copyright']."</copyright>\n";
  echo "     <language>".$CONF['feed.language']."</language>\n";
  echo "     <pubDate>$pubDate</pubDate>\n";
  echo "     <docs>http://blogs.law.harvard.edu/tech/rss</docs>\n";
  echo "     <generator>blogRight!</generator>\n";
  echo "     <managingEditor>".$CONF['feed.editor']."</managingEditor>\n";

  foreach ($include_files as $inc)
  {
    if (ereg (".*\.html\$", basename ($inc)))
    {
      $cdd = basename (dirname ($inc)); // current data dir
      if (is_numeric ($cdd))
      {
	  ereg ('^(.*)-(.*)\.', basename ($inc), $res);
	  $year = $cdd; // let's assume that...
	  
	  if (isset ($res[1]) && isset ($res[2]))
	  {
	    $month = $res[1];
	    $day   = $res[2];
	  }

	  $fmtime = mktime (0, 0, 0, $month, $day, $year);
      }
      else {
	$fmtime = filemtime ($inc);
      }

      $pubDate   = date ("D, d M Y H:i:s O", $fmtime);
      $idx = substr ($inc, strlen ($cwd));
      $itemTitle = isset ($feed_titles_idx[$idx]) ? $feed_titles_idx[$idx] :
	date ('d M Y', $fmtime);
      $permaLink = date ($CONF['permalinkFmt'], $fmtime);

      echo "     <item>\n";
      echo "       <title>$itemTitle</title>\n";
      echo "       <link>".$CONF['url.site']."/".
	$CONF['qpattern'].$permaLink."</link>\n";

      if ($CONF['feed.CDATA']) {
	echo "       <description><![CDATA[\n";
	indent_include (0, $inc, $CONF['feed.CDATA']);
	echo "]]></description>\n";
      }
      else {
	echo "       <description>\n";
	indent_include (0, $inc, $CONF['feed.CDATA']);
	echo "       </description>\n";
      }

      echo "       <guid isPermaLink=\"true\">".
	$CONF['url.site']."/".$CONF['qpattern'].
	$permaLink."</guid>\n";
      echo "       <pubDate>$pubDate</pubDate>\n";
      echo "     </item>\n";
    }
  }

  echo "  </channel>\n";
  echo "</rss>\n";
}
else if (isset ($ATOM))
{
  $pubDate = gmdate ('Y-m-d\TH:i:s\Z', filemtime ($include_files[0]));
  $lastModified = gmdate ("D, d M Y H:i:s T", filemtime ($include_files[0]));
  $ETag = '"'.md5 ($lastModified).'"';

  $headers = getallheaders ();
  if (array_key_exists ('If-Modified-Since', $headers) &&
      array_key_exists ('If-None-Match', $headers))
  {
    if ($headers['If-Modified-Since'] == $lastModified &&
	$headers['If-None-Match'] == $ETag)
    {
      header ('HTTP/1.1 304 Not Modified');
      exit ();
    }
  }
  header ("Content-Type: text/xml; charset=".$CONF['charsetEncoding']);
  header ("Last-Modified: $lastModified");
  header ("ETag: $ETag");

  echo '<?xml version="1.0" encoding="'.$CONF['charsetEncoding'].'"?>'."\n";
  echo '<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="'.
    $CONF['feed.language'].'">'."\n";
  echo "  <title>".$CONF['feed.title']."</title>\n";
  echo "  <subtitle>".$CONF['feed.description']."</subtitle>\n";
  echo "  <updated>$pubDate</updated>\n";
  echo "  <author>\n";
  echo "    <name>".$CONF['feed.authorName']."</name>\n";
  echo "    <email>".$CONF['feed.authorEmail']."</email>\n";
  echo "  </author>\n";
  echo "  <rights>".$CONF['feed.copyright']."</rights>\n";
  echo "  <generator>blogRight!</generator>\n";
  echo "  <id>".$CONF['feed.tagUri']."</id>\n";
  echo "  <link rel=\"self\" type=\"application/atom+xml\" href=\"".
    $CONF['url.site']."/atom/\"/>\n";
  echo "  <link rel=\"alternate\" type=\"text/html\" href=\"".
    $CONF['url.site']."/\"/>\n";

  foreach ($include_files as $inc)
  {
    if (ereg (".*\.html\$", basename ($inc)))
    {
      $cdd = basename (dirname ($inc)); // current data dir
      if (is_numeric ($cdd))
      {
	  ereg ('^(.*)-(.*)\.', basename ($inc), $res);
	  $year = $cdd;
	  
	  if (isset ($res[1]) && isset ($res[2]))
	  {
	    $month = $res[1];
	    $day   = $res[2];
	  }

	  $fmtime = mktime (0, 0, 0, $month, $day, $year);
      }
      else {
	$fmtime = filemtime ($inc);
      }

      $pubDate   = date ('Y-m-d\TH:i:s\Z', $fmtime);
      $idx = substr ($inc, strlen ($cwd));
      $itemTitle = isset ($feed_titles_idx[$idx]) ? $feed_titles_idx[$idx] :
	date ('d M Y', $fmtime);
      $permaLink = date ($CONF['permalinkFmt'], $fmtime);

      echo "  <entry>\n";
      echo "    <id>".$CONF['feed.tagUri']."/$permaLink</id>\n";
      echo "    <updated>$pubDate</updated>\n";
      echo "    <title>$itemTitle</title>\n";
      echo "    <link rel=\"alternate\" type=\"text/html\" href=\"".
	$CONF['url.site']."/".$CONF['qpattern'].$permaLink."\"/>\n";

      if ($CONF['feed.XHTML']) {
	echo "    <content type=\"xhtml\" xml:space=\"preserve\">\n";
	echo "    <div xmlns=\"http://www.w3.org/1999/xhtml\">\n";
	indent_include (0, $inc, $CONF['feed.XHTML']);
	echo "    </div>\n";
	echo "    </content>\n";
      }
      else if ($CONF['feed.CDATA']) {
	echo "    <content type=\"html\" xml:space=\"preserve\"><![CDATA[\n";
	indent_include (0, $inc, $CONF['feed.CDATA']);
	echo "]]></content>\n";
      }
      else {
	echo "    <content type=\"html\" xml:space=\"preserve\">\n";
	indent_include (0, $inc, $CONF['feed.CDATA']);
	echo "    </content>\n";
      }

      echo "  </entry>\n";
    }
  }

  echo '</feed>'."\n";
}
else // HTML
{
  echo '<?xml version="1.0" encoding="'.$CONF['charsetEncoding'].'"?>'."\n";

  $title = '';
  if (isset ($search_y)) {
    $title = $search_y;
    if (isset ($search_m))
      $title .= '-'.$search_m;
    if (isset ($search_d))
      $title .= '-'.$search_d;
  }

  @include ($CONF['path.header_file']);

  echo "\n<div id=\"content\" class=\"hfeed\">\n\n";

  foreach ($include_files as $inc)
  {
    // the query engine...
    if (isset ($search_m))
    {
      if (isset ($search_d))
      {
	if (ereg ("^$search_m-$search_d.*\.html\$", basename ($inc)))
	  include_file ($inc, 'd');
      }
      else
	if (ereg ("^$search_m-.*\.html\$", basename ($inc)))
	  include_file ($inc, 'm');
    }
    else
      if (ereg (".*\.html\$", basename ($inc)))
	include_file ($inc, 'y');
  }

  // Calculate a copyright footnote.
  $copyrightYear = '';
  if (count ($year_array))
  {
    $year_array = array_unique ($year_array);
    sort ($year_array);
    foreach ($year_array as $y)
      $copyrightYear .= "$y, ";
    $copyrightYear = substr ($copyrightYear, 0, -2);
  }
  else
    $copyrightYear = date ('Y');

  echo "</div>\n\n"; /* /hfeed */

  @include ($CONF['path.footer_file']);

  $duration = microtime_diff ($start_time, microtime ());
  $duration = sprintf ("%0.6f", $duration);
  echo "\n<!-- processing took $duration seconds -->";
  echo "\n<!-- powered by blogRight! -->\n";
}

/////////////////////////////

$footnotes = array (); // Array of footnote texts. Indexed by $local_fn
$local_fn  = 0;        // Local (within the entry) number of the next footnote. 
$global_fn = 0;        // Global number of the next footnote. This is unique
                       // within a html page and is used for creating unique
                       // anchors.

// Return full url of the script
function script_url ()
{
  global $CONF;
  if ($CONF['qpattern'] != '' && isset ($_GET['q']))
    return $CONF['url.site'].'/'.$CONF['qpattern'].$_GET['q'];
  return $CONF['url.site'].'/';
}

// Save away the collected footnote text and replace it by an appropriate
// link.
function footnote_handler ($matches)
{
  global $global_fn, $local_fn, $footnotes;
  $footnotes[$local_fn++] = $matches[1];
  $global_fn++;
  return '<a href="'.script_url ().'#footnote'.$global_fn.'" name="FNRET'.
    $global_fn.'"><sup>'.$local_fn.')</sup></a> ';
}

// Process a single input line from the file being included.
// Returns processed line, if it is ready for output. Otherwise,
// tucks the line to the global variable $collect and returns false.
// Make sure you unset $collect before the *first* invocation of process_line()
// for the given input file.
function process_line ($line)
{
  global $CONF, $search_d, $local_fn, $global_fn;
  global $footnotes, $collect;

  if (isset ($collect))
  {
    $collect = $collect . ' ' . rtrim ($line);

    if (!preg_match ('^</footnote>^', $line))
      return false;
    $line = $collect . "\n";
    unset ($GLOBALS['collect']);
  }

  $line = preg_replace_callback ('^<footnote>(.*)</footnote>^',
				 "footnote_handler", $line);

  if (preg_match ('^<footnote>^', $line))
  {
    $collect = rtrim ($line);
    return false;
  }

  if (!preg_match ('/="(https?|ftp|mailto):\/\/.*?"/i', $line)) // a local path
  {
    $res = array ();

    if (preg_match ('/<img src="(.*?)"(.*?)>/i', $line, $res))
    {
      $line = str_replace ('\"', '"',
			   preg_replace ('/<img src="(.*?)"(.*?)>/ie',
					 "str_replace(array('%IMAGE','%PARAMS'),array('\\1','\\2'),'".
					 $CONF['patternURL.img']."')", $line));
    }
    else if (preg_match ('/<photo album="(.*?)" src="(.*?)"(.*?)>/i',
			 $line, $res))
    {
      if ($res[1] == 'default')
	$res[1] = $CONF['defaultGallery'];

      $prefix = dirname (trim ($res[2]));
      if ($prefix[0] == '.')
	$prefix = $CONF['thumbnailPrefix'][$res[1]];
      $photo = basename (trim ($res[2]));
      $thumbnail = $prefix . $photo;
      $photo = str_replace ('.jpg', '', $photo);

      $line = '<p class="photo">'.
	str_replace (array ('%ALBUM', '%PHOTO', '%THUMBNAIL', '%PARAMS'),
		     array ($res[1], $photo, $thumbnail, $res[3]),
		     $CONF['patternURL.photos']).'</p>';
    }
    else if (preg_match ('/<photo src="(.*?)"(.*?)>/i', $line, $res))
    {
      $line = '<p class="photo">'.
	str_replace (array ('%PHOTO', '%THUMBNAIL', '%PARAMS'),
		     array ($res[1],
			    $CONF['thumbnailPrefix']['default'].$res[1],
			    $res[2]),
		     $CONF['patternURL.photo']).'</p>';
    }
    else if (preg_match ('/<gallery album="(.*?)" img="(.*?)"(.*?)>/i',
			 $line, $res))
    {
      $pdir = dirname (trim ($res[2]));
      if ($pdir[0] == '.')
	$pdir = '';
      $pbase = basename (trim ($res[2]));
      $thumbnail = '';
      if ($pdir) $thumbnail .= $pdir.'/';
      $thumbnail .= $CONF['thumbnailPrefix']['default'].$pbase;

      $line = '<p class="gallery">'.
	str_replace (array ('%ALBUM', '%THUMBNAIL', '%PARAMS'),
		     array ($res[1], $thumbnail, $res[3]),
		     $CONF['patternURL.gallery'])."</p>\n";
    }
    $line = str_replace ('\"', '"',
			 preg_replace ('/<a href="(.*?)">/ie',
				       "str_replace(array('%URI'),array('\\1'),'".
				       $CONF['patternURL.link']."')", $line));
  }
  return $line;
}

// Process the footnotes collected for the entry.
// Arguments:
//  $out - name of the output function. The function must be
// declared as 'function foo($data, $string). It is passed $data closure
// and the $string to be output.
//  $data - Any function-specific data $out may need. 
function process_footnotes ($out, $data)
{
  global $local_fn, $global_fn, $footnotes;

  if ($local_fn > 0)
  {
    $out ($data, '<div class="footnote">');
    for ($i = 0; $i < $local_fn; $i++)
    {
      $n = $global_fn-$local_fn+$i+1;
      $out ($data, '<p><a href="'.script_url().'#FNRET'.
	    $n.'" name="footnote'.$n.'"><sup>'.($i+1).')</sup></a> ');
      $out ($data, $footnotes[$i]);
      $out ($data, '</p>');
    }
    $out ($data, '</div>');
  }
}

// Simple printer (alias for print, to be used with process_footnotes)
function printer ($data, $string)
{
  echo $string;
}

/////////////////////////////

// HTML output
function include_file ($file, $type)
{
  global $CONF, $search_y, $search_m, $local_fn, $year_array;
  $res = array ();
  $cdd = basename (dirname ($file)); // current data dir

  $local_fn = 0;
  if (is_numeric ($cdd))
  {
    ereg ('^(.*)-(.*)\.', basename ($file), $res);
    $year = $cdd; // assume that...

    $year_array[] = intval ($cdd);

    if (isset ($res[1]) && isset ($res[2]))
    {
      $month = $res[1];
      $day   = $res[2];
    }

    $entryDate = mktime (0, 0, 0, $month, $day, $year);
    $permaLink = date ($CONF['permalinkFmt'], $entryDate);
    $pubDate   = date ('Y-m-d\TH:i:s\Z', $entryDate);
    $entryDate = date ('d M Y', $entryDate);
  }
  else
  {
    $mt = filemtime ($file);
    $permaLink = date ($CONF['permalinkFmt'], $mt);
    $pubDate   = gmdate ('Y-m-d\TH:i:s\Z', $mt);
    $entryDate = date ('d M Y', $mt);
  }

  $fd = file ($file);
  if ($fd)
  {
    echo '<div id="e'.str_replace ( '/','',$permaLink).'" class="hentry">'."\n";
    echo '<div class="entry-title">';
    echo '<a href="./'.$CONF['qpattern'].$permaLink
      .'" rel="bookmark" title="'.$entryDate.'">'.$entryDate.'</a>';
    echo '</div>'."\n";
    echo '<div class="entry-content">'."\n";

    unset ($GLOBALS['collect']); 
    foreach ($fd as $line)
    {
      $line = process_line ($line);
      if ($line)
	echo ($line);
    }
    process_footnotes ('printer', 0);
    echo '</div>'."\n";

    echo '<div class="entry-footer">'."\n";
    echo '<a href="./'.$CONF['qpattern'].
      $permaLink.'" class="bookmark" rel="bookmark" title="'.
      $entryDate.'"><abbr class="published" title="'.
      $pubDate.'">'.$entryDate.'</abbr></a>'."\n";
    echo '<address class="vcard"><span class="fn">'.
      $CONF['feed.authorName'].'</span></address>';

    /* disqus support */
    if (isset ($CONF['disqus.account']) && $CONF['disqus.account'] != '') {
      if ($type == 'd') {
	echo "\n";
	echo '<div id="disqus_thread"></div>';
	echo '<script type="text/javascript" src="http://disqus.com/forums/'.
	  $CONF['disqus.account'].'/embed.js"></script>';
	if (isset ($CONF['disqus.showNoScript'])) {
	  echo '<noscript><div><a href="http://'.$CONF['disqus.account'].
	    '.disqus.com/?url=ref">View the forum thread.</a></div></noscript>';
	}
	echo '<a href="http://disqus.com" class="dsq-brlink">'.
	  'comments powered by <span class="logo-disqus">Disqus</span>'.
	  '</a>'."\n";
      }
      else if (isset ($CONF['recent']) &&
	       (!isset ($search_y) || isset ($search_m))) {
	echo '<a href="./'.$CONF['qpattern'].$permaLink.
	  '#disqus_thread" title="View comments">Comments</a>'."\n";
      }
    }

    echo '</div>'."\n";
    echo '</div>'."\n\n";
  }
}

function printer_html ($level, $string)
{
  for ($i = 0; $i < $level; $i++)
    echo ' ';
  echo (htmlentities ($string) . "\n");
}

// Atom/RSS 2.0 output
function indent_include ($level, $file, $unescaped)
{
  global $local_fn;
  $local_fn = 0;
  $cdd = basename (dirname ($file)); // current data dir
  $fd  = file ($file);
  if ($fd)
  {
    unset ($GLOBALS['collect']);
    foreach ($fd as $line)
    {
      $line = process_line ($line);
      if ($line)
      {
	for ($i = 0; $i < $level; $i++)
	  echo ' ';
	if ($unescaped)
	  echo $line;
	else
	  echo htmlspecialchars (htmlentities2 ($line));
      }
    }

    process_footnotes ('printer_html', $level);
  }
}

function printCalendar ()
{
  global $CONF, $cwd, $cur_y, $cur_m;

  echo "<!-- BEGIN CALENDAR -->\n\n";

  if (!isset ($CONF['calendar.emptyMonths']))
    $CONF['calendar.emptyMonths'] = true;

  if (!isset ($CONF['calendar.vertical']))
    $CONF['calendar.vertical'] = false;

  $searchdirs = array ();
  if (!($dp = opendir ($cwd)))
    die ("Can't open data directory.");
  while ($name = readdir ($dp))
  {
    if (is_numeric ($name) && $name <= $cur_y && is_dir ($cwd.'/'.$name))
      $searchdirs[] = $cwd.'/'.$name;
  }
  closedir ($dp);
  sort ($searchdirs);

  echo "<div id=\"calendar\">\n";
  echo "<table class=\"calendar-table\"><tr>\n";

  foreach ($searchdirs as $dir)
  {
    if (!($dp = opendir ($dir)))
      continue;

    $dir = basename ($dir);

    echo "\n  <!-- $dir -->\n";
    echo "  <td><div class=\"calendar-year\"><a href=\"./".
      $CONF['qpattern'].$dir."/\" rel=\"nofollow\">$dir</a></div>\n";
    echo "  <table class=\"calendar-months\">\n\n";

    $mtab = array ();
    while ($name = readdir ($dp))
    {
      if (ereg ("^(.*)-(.*)\.html\$", $name, $res))
      {
	if (!($dir >= $cur_y && $res[1] > $cur_m))
	  $mtab[0 + $res[1]]++;
      }
    }
    closedir ($dp);
  
    for ($month = 1, $row = 0; $row < 4; $row++)
    {
      echo "  <tr>\n";
      for ($col = 0; $col < 3; $col++, $month++)
      {
	echo "   <td>";
	if ($mtab[$month])
	{
	  printf (" <a href=\"./".
		  $CONF['qpattern']."%4d/%02d/\">",
		  $dir, $month);
	  echo strftime ("%b", strtotime ("$dir-$month-1"));
	  echo "</a> ";
	}
	else
	{
	  if ($CONF['calendar.emptyMonths'])
	    echo strftime ("%b", strtotime ("$dir-$month-1"));
	  else
	    echo ' &nbsp; ';
	}
	echo "</td>\n";
      }
      echo "  </tr>\n\n";
    }
  
    echo "  </table></td>\n";
    
    if ($CONF['calendar.vertical'])
      echo "  </tr><tr>\n";
  }

  echo "\n</tr>\n</table>\n";
  echo "</div>\n";
  echo "\n<!-- END CALENDAR -->\n\n";
}

function htmlentities2 ($htmlcode) {
  static $htmlEntities;
  static $entitiesDecoded;
  static $utf8Entities;
  if (!isset ($htmlEntities))
    $htmlEntities = array_values (get_html_translation_table (HTML_ENTITIES,
							      ENT_QUOTES));
  if (!isset ($entitiesDecoded))
    $entitiesDecoded = array_keys (get_html_translation_table (HTML_ENTITIES,
							       ENT_QUOTES));
  if (!isset ($utf8Entities)) {
    $num = count ($entitiesDecoded);
    for ($u = 0; $u < $num; $u++)
      $utf8Entities[$u] = '&#'.ord ($entitiesDecoded[$u]).';';
  }
  return str_replace ($htmlEntities, $utf8Entities, $htmlcode);
}

function microtime_diff ($a, $b)
{
  list ($a_dec, $a_sec) = explode (' ', $a);
  list ($b_dec, $b_sec) = explode (' ', $b);
  return $b_sec - $a_sec + $b_dec - $a_dec;
}

?>
